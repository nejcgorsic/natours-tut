/* eslint-disable */
import axios from 'axios';
import { showAlert } from './alerts';


export const bookTour = async tourId => {
  try {
    const stripe = Stripe('pk_test_51Ip6EyFIWCsv3zFu2sEDK7mfRBEC6rMXBhsTu0NYs4s0UWuvRB2L3sWBRMB32TdQ47Vui8M5hTFd5PyYIQNfWIaI007TlEYzhX');
    // 1) Get the checkout session from API
    const session = await axios(`/api/v1/bookings/checkout-session/${tourId}`)
    // 2) Create checkout form + charge credit card
    await stripe.redirectToCheckout({
      sessionId: session.data.session.id
    });

  } catch (err) {
    showAlert('error', err);
  }

}