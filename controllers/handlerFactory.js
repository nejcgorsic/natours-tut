'use strict'

const AppError = require('../utils/appError');
const APIFeatures = require('./../utils/apiFeatures');
const catchAsync = require('./../utils/catchAsync');

const deleteOne = Model => catchAsync(async (req, res, next) => {
  const paramsId = req.params.id;
  const doc = await Model.findByIdAndDelete(paramsId);

  if (!doc) return next(new AppError(`No document found with id: ${paramsId}`, 404));

  res.status(204).json({
    status: 'success',
    data: null
  });
});

const updateOne = Model => catchAsync(async (req, res, next) => {
  const paramsId = req.params.id;

  const doc = await Model.findByIdAndUpdate(paramsId, req.body, {
    new: true,
    runValidators: true
  });

  if (!doc) return next(new AppError(`Tour not found with id: ${paramsId}`, 404));

  res.status(200).json({
    status: 'success',
    data: {
      data: doc
    }
  })
});

const createOne = Model => catchAsync(async (req, res, next) => {
  const doc = await Model.create(req.body);
  res.status(201).json({
    status: 'success',
    data: {
      data: doc
    }
  });

  // try {
  // } catch (err) {
  //   res.status(400).json({
  //     status: 'fail',
  //     message: err
  //   })
  // }
});

const getOne = (Model, populateOptions) => catchAsync(async (req, res, next) => {
  const paramsId = req.params.id;
  // Populate fields guides with the actual data (guides has an ObjectID ref. in DB.)
  // add populate if there is some
  let query = Model.findById({ _id: paramsId });
  if (populateOptions) query = query.populate(populateOptions);

  const doc = await query;
  if (!doc) {
    return next(new AppError(`Document not found with id: ${paramsId}`, 404));
  }

  res.status(200).json({
    status: 'success',
    data: {
      data: doc
    }
  })
});

const getAll = Model => catchAsync(async (req, res, next) => {
  // TO ALLOW FOR NESTED GET REVIEWS ON TOUR (hack)
  let filter = {};
  if (req.params.tourId) filter = { tour: req.params.tourId };
  // EXECUTE QUERY
  const features = new APIFeatures(Model.find(filter), req.query)
    .filter().sort().limitFields().paginate();

  const doc = await features.query;
  // const doc = await features.query.explain();

  // SEND RESPONSE
  res.status(200).json({
    status: 'success',
    results: doc.length,
    data: {
      data: doc
    }
  });
});

module.exports = {
  deleteOne,
  updateOne,
  createOne,
  getOne,
  getAll
}
