module.exports = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Swagger for natours API",
      version: "0.1.0",
      description:
        "Swagger for natours tutorial"
    },
    servers: [
      {
        url: "http://localhost:3000/api",
      },
    ],
  },
  apis: ["./routes/tourRoutes.js"],
};