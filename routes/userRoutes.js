const express = require('express');
const multer = require('multer');
const {
  getAllUsers,
  createUser,
  getUser,
  updateUser,
  deleteUser,
  updateMe,
  deleteMe,
  getMe,
  uploadUserPhoto,
  resizeUserPhoto
} = require('./../controllers/userController')

const { signup,
  login, forgotPassword, resetPassword,
  updatePassword, protect, restrictTo,
  logout } = require('./../controllers/authController')

// ROUTES  -------------------------------------------------------------------------------
// app.get('/api/v1/tours', getAllTours);
// app.get('/api/v1/tours/:id', getTour);
// app.post('/api/v1/tours', createTour);
// app.patch('/api/v1/tours/:id', updateTour);
// app.delete('/api/v1/tours/:id', deleteTour);



const router = express.Router();

router.post('/signup', signup);
router.post('/login', login);
router.get('/logout', logout);
router.post('/forgotPassword', forgotPassword);
router.patch('/resetPassword/:token', resetPassword);


router.use(protect); // ------------------------- PROTECT ALL ROUTES BELOW! (This is middleware!)

router.patch('/updateMyPassword', updatePassword);
router.get('/me', getMe, getUser)
router.patch('/updateMe', uploadUserPhoto, resizeUserPhoto, updateMe);
router.delete('/deleteMe', deleteMe);


router.use(restrictTo('admin'));

router
  .route('/')
  .get(getAllUsers)
  .post(createUser);
router
  .route('/:id')
  .get(getUser)
  .patch(updateUser)
  .delete(deleteUser);

// POST /tour/<tour id>/reviews
// GET /tour/<tour id>/reviews
// GET /tour/<tour id>/reviews/<review id>


module.exports = router;