const express = require('express');
const {
  getAllTours, getTour,
  createTour, updateTour, deleteTour, aliasTopTours,
  getTourStats, getMonthlyPlan,
  getToursWithin, getDistances,
  resizeTourImages, uploadTourImages
} = require('./../controllers/tourController')
const { protect, restrictTo } = require('./../controllers/authController');
const router = express.Router();
// const { createReview } = require('./../controllers/reviewController')
const reviewRouter = require('./../routes/reviewRoutes');

/**
 * @swagger
 * /top-5-cheap:
 *   get:
 *     summary: Retrieve a list of cheapest 5 tours
 *     description: placeholder
 * /tour-stats:
 *   get:
 *     summary: Get tour stats 
 *     description: placeholder
 * /monthly-plan/:year:
 *   get:
 *     summary: Get monthly plan based on year
 *     description: placeholder
 * /:
 *   get:
 *     summary: Get all tours
 *     description: placeholder
 *   post:
 *     summary: Create a tour
 *     description: placeholder
 * /:id:
 *   get:
 *     summary: Get a specific tour
 *     description: placeholder
 *   patch:
 *     summary: Update a specific tour
 *     description: placeholder
 *   delete:
 *     summary: Delete a specific tour
 *     description: placeholder
*/

router.use('/:tourId/reviews', reviewRouter);

router.route('/top-5-cheap')
  .get(aliasTopTours, getAllTours);

router.route('/tour-stats')
  .get(getTourStats);

router.route('/monthly-plan/:year')
  .get(protect, restrictTo('admin', 'lead-guide', 'guide'), getMonthlyPlan);


router.route('/tours-within/:distance/center/:latlng/unit/:unit')
  .get(getToursWithin);

router.route('/distances/:latlng/unit/:unit')
  .get(getDistances);

router.route('/')
  // .get(protect, getAllTours)
  .get(getAllTours) // -- expose API.
  .post(protect, restrictTo('admin', 'lead-guide'), createTour);

router.route('/:id')
  .get(getTour)
  .patch(protect, restrictTo('admin', 'lead-guide'), uploadTourImages, resizeTourImages, updateTour)
  .delete(protect, restrictTo('admin', 'lead-guide'), deleteTour);

// router.route('/:tourId/reviews')
//   .post(protect, restrictTo('user'), createReview)

module.exports = router;
