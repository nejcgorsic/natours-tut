/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */
const express = require('express');
const { getOverview, getTour, loginUser, getAccount, updateUserData, getMyTours } = require('./../controllers/viewsController')
const { protect, isLoggedIn } = require('./../controllers/authController');
const bookingController = require('./../controllers/bookingController');
const router = express.Router();

router.get('/', bookingController.createBookingCheckout, isLoggedIn, getOverview);
router.get('/tour/:tourSlug', isLoggedIn, getTour);
router.get('/login', isLoggedIn, loginUser);
router.get('/me', protect, getAccount);
router.get('/my-tours', protect, getMyTours);
router.post('/submit-user-data', protect, updateUserData);


module.exports = router;