const mongoose = require('mongoose');
const Tour = require('./tourModel');

/** Review
 * rating
 * createdAt
 * ref to tour
 * ref to user
 */

const reviewSchema = new mongoose.Schema({
  review: {
    type: String,
    required: [true, 'A review cannot be empty']
  },
  rating: {
    type: Number,
    min: 1,
    max: 5,
    required: [true, 'A review must have a rating']
  },
  createdAt: {
    type: Date,
    default: Date.now()
  },
  tour: {
    type: mongoose.Schema.ObjectId,
    ref: 'Tour',
    required: [true, 'A review must ref to a tour']
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    required: [true, 'A review must ref to a user']
  },
}, {
  toJSON: {
    virtuals: true,
    toObject: true
  }
});

// This will prevent from users posting multiple reviews from the same user.
reviewSchema.index({ tour: 1, user: 1 }, { unique: true });

reviewSchema.pre(/^find/, function (next) {
  this.populate({
    path: 'user',
    select: 'name photo'
  })

  next();
})

reviewSchema.statics.calcAverageRating = async function (tourId) {
  const stats = await this.aggregate([
    {
      $match: { tour: tourId }
    },
    {
      $group: {
        _id: '$tour',
        nRating: { $sum: 1 },
        avgRating: { $avg: '$rating' }
      }
    }
  ]);

  // This only adds a review ? What about updating and creating?!!
  if (stats.length > 0) {
    await Tour.findByIdAndUpdate(tourId, {
      ratingsQuantity: stats[0].nRating,
      ratingsAverage: stats[0].avgRating
    })
  } else {
    await Tour.findByIdAndUpdate(tourId, {
      ratingsQuantity: 0,
      ratingsAverage: 4.5
    })
  }
}

// After the document is saved into DB, it makes sense to save it into the database.
reviewSchema.post('save', function () {
  // this points to the current model (review)!
  this.constructor.calcAverageRating(this.tour);
});

// this is to update ratingsAverage if you update or delete a review.
// We use PRE here because we need to update it before it gets saved.
reviewSchema.pre(/^findOneAnd/, async function (next) {
  // Create a property on this variable.
  // pass data from PRE middleware to POST middleware.
  this.r = await this.findOne();
  next();
});

// Perfect time to call calcAverageRating
reviewSchema.post(/^findOneAnd/, async function () {
  // Review from pre function.
  // this.r = await this.findOne(); --> This does not WORK here, query has already executed.
  await this.r.constructor.calcAverageRating(this.r.tour);
});

const Review = mongoose.model('Review', reviewSchema);

module.exports = Review;
// POST /tour/<tour id>/reviews
// GET /tour/<tour id>/reviews
// GET /tour/<tour id>/reviews/<review id>
// NESTED ROUTE ^^^^^^^^^^^^^
// review is child of tour.